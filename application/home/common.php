<?php

/**
 * 生成url函数
 * @param $self
 * @param $parmas
 * @return string
 */
function createUrl($self,$parmas){
    $data=[];
    $parmas['page']=1;
    foreach ($parmas as $k=>$v){
        if($self[0]!==$k){
            if($v!=""){
                $data[$k]=$v;
            }
        }else{
            $data[$k]=$self[1];
        }
    }
    $url=\think\Url::build('home/filter/index',$data);
    return $url;
}

/**分页函数
 * @param $current
 * @param $total
 * @param $params
 */
function pagenation($current,$total,$params){
    if($total<=11){
        $params['page']=1;
        echo "<a href=".url('home/filter/index',$params).">首页</a>";
        for($i=1;$i<=$total;$i++){
            $params['page']=$i;
            if($i==$current){
                echo "<span class='active'>$i</span>";
            }else{
                echo "<a href=".url('home/filter/index',$params).">$i</a>";
            }
        }
        $params['page']=$total;
        echo "<a href=".url('home/filter/index',$params).">末页</a>";
        return;
    }
    if($current<=6){
        $params['page']=1;
        echo "<a href=".url('home/filter/index',$params).">首页</a>";
        for($i=1;$i<=11;$i++){
            $params['page']=$i;
            if($i==$current){
                echo "<span class='active'>$i</span>";
            }else{
                echo "<a href=".url('home/filter/index',$params).">$i</a>";
            }
        }
        $params['page']=$total;
        echo "<a href=".url('home/filter/index',$params).">末页</a>";
    }else{
        $params['page']=1;
        echo "<a href=".url('home/filter/index',$params).">首页</a>";
        $end=$current+5;
        $start=$current-5;
        if($current>$total-10){
            $end=$total;
            $start=$total-10;
        }
        for($i=$start;$i<=$end;$i++){
            $params['page']=$i;
            if($i==$current){
                echo "<span class='active'>$i</span>";
            }else{
                echo "<a href=".url('home/filter/index',$params).">$i</a>";
            }
        }
        $params['page']=$total;
        echo "<a href=".url('home/filter/index',$params).">末页</a>";
    }
}

/**主演分割
 * @param $str
 * @return mixed
 */
function splitActors($str){
    return str_replace("|","、",$str);
}

/**
 * 生成链接
 * @param $str
 */
function spliteClass($str){
    $arr=explode(" ",$str);
    for($i=3;$i<count($arr);$i++){
        echo "/<a href=".url('home/filter/index',['class'=>$arr[$i]]).">$arr[$i]</a>";
    }
}

/**
 * 搜索也关键词
 * @param $params
 * @return string
 */
function createKeywords($params){
    $nav=$params['type'];
    $class=$params['class'];
    $area=$params['area'];
    $page=$params['page'];

    return "欢乐影视 BT天堂 $nav $area 第 $page 页 $nav $class 下载 BT下载 迅雷下载 电影下载";
}

function detailKeywords($detail){
    $title=$detail['title'];
    $abstract=$detail['abstract'];
    $area=$detail['area'];
    $nav=$detail['nav'];

    return "欢乐影视 BT天堂 ".$title."下载 $abstract $area $nav 下载 电影下载 BT下载";
}

function changeImgUrl($url){
    if(strpos($url,"http")){
        return $url;
    }else{
        return "/static/images/cover/$url";
    }
}

/**
 * 验证传入的参数中是否含有注入字符
 * @param $str1
 * @return false|int
 */
function inject_check($str1) {
    if(preg_match('/select|insert|and|or|update|delete|union|into|load_file|outfile|from|count\(|drop table|update|truncate|asc\(|mid\(|char\(|xp_cmdshell|exec|master|\/\*|\*|\.\.\/|\.\//i', $str1)){
        return;
    }else{
        return $str1;
    }
}

function keywordsTitle($name){
    return "$name ".config("keywords");
}

function splitAbstract($str){
    return explode(" ",$str);
}

function searchPage($page,$total,$key){
    if($total<=11){
        echo "<a href=".url('home/search/index',['page'=>1,'keywords'=>$key]).">首页</a>";
        for($i=1;$i<=$total;$i++){
            if($i==$page){
                echo "<span class='active'>$i</span>";
            }else{
                echo "<a href=".url('home/search/index',['page'=>$i,'keywords'=>$key]).">$i</a>";
            }
        }
        echo "<a href=".url('home/search/index',['page'=>$total,'keywords'=>$key]).">末页</a>";
        return;
    }
    if($page<=6){
        echo "<a href=".url('home/search/index',['page'=>1,'keywords'=>$key]).">首页</a>";
        for($i=1;$i<=11;$i++){
            if($i==$page){
                echo "<span class='active'>$i</span>";
            }else{
                echo "<a href=".url('home/search/index',['page'=>$i,'keywords'=>$key]).">$i</a>";
            }
        }
        echo "<a href=".url('home/search/index',['page'=>$total,'keywords'=>$key]).">末页</a>";
    }else{
        echo "<a href=".url('home/search/index',['page'=>1,'keywords'=>$key]).">首页</a>";
        $end=$page+5;
        $start=$page-5;
        if($page>$total-10){
            $end=$total;
            $start=$total-10;
        }
        for($i=$start;$i<=$end;$i++){
            if($i==$page){
                echo "<span class='active'>$i</span>";
            }else{
                echo "<a href=".url('home/search/index',['page'=>$i,'keywords'=>$key]).">$i</a>";
            }
        }
        echo "<a href=".url('home/search/index',['page'=>$total,'keywords'=>$key]).">末页</a>";
    }
}