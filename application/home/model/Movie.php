<?php

namespace app\home\model;

use think\Db;
use think\Model;

class Movie extends Model
{
    /**
     * 获取大类下最新更新的电影
     * @param $nav 电影类型
     * return array
     */
    public function getNewUpdateMovies($nav)
    {
        $res = [
            $nav => []
        ];
        if ($nav) {
            $mid = model('MovieClassify')->where('nav', '=', $nav)->order('update_time desc')->limit(10)->column('id,mid');
            $v = [];
            foreach ($mid as $val) {
                array_push($v, $val);
            }
            $vs = implode(",", $v);
            $query = $this->where('mid', 'in', "($vs)")
                ->column('id,mid,year,area,lang,title,cover,update_state,rating');
            if ($query) {
                foreach ($query as $item) {
                    array_push($res[$nav], $item);
                }
            }
        }
        return $res;
    }

    /**
     * @param $nav 主类型
     * @param $type 主题类型
     * @param $area 地区
     * @param $year 年份
     * @param $lang 语言
     * @param $sort 排序
     * @param $num 数量
     * @return array|mixed
     */
    public function getMoviesByCondition($nav,$type, $area, $year, $lang, $sort,$num=10)
    {
        $str = [];
        if ($nav) {
            array_push($str, "`nav`='$nav'");
        }
        if ($type) {
            array_push($str, "`type`='$type'");
        }
        if ($area) {
            array_push($str, "`area`='$area'");
        }
        if ($year) {
            array_push($str, "`year`='$year'");
        }
        if ($lang) {
            array_push($str, "`lang`='$lang'");
        }
        //插寻sql
        $sql = implode(" AND ", $str);
//        dump($sql);
        if($sql){
            if ($sort == 'desc') {
                $query = Db::query("SELECT `mid`,`update_time` FROM `dybt_movie_classify` WHERE $sql GROUP BY `mid` ORDER BY `update_time` DESC limit $num");
            }
            if ($sort == 'asc'){
                $query = Db::query("SELECT `mid`,`update_time` FROM `dybt_movie_classify` WHERE $sql GROUP BY `mid` ORDER BY `update_time` ASC limit $num");
            }
        }else{
            if ($sort == 'desc') {
                $query = Db::query("SELECT `mid`,`update_time` FROM `dybt_movie_classify`  GROUP BY `mid` ORDER BY `update_time` DESC limit $num");
            }
            if ($sort == 'asc'){
                $query = Db::query("SELECT `mid`,`update_time` FROM `dybt_movie_classify` GROUP BY `mid` ORDER BY `update_time` ASC limit $num");
            }
        }

        if ($query) {
            $midsArr=[];
            foreach ($query as $item){
                array_push($midsArr,$item['mid']);
            }
            $midsStr = implode(",", $midsArr);
            if($midsStr!=""){
                $queryMovies = Db::query("SELECT `mid`, `title`, `cover`, `update_state`,`rating`,`abstract` FROM `dybt_movie` WHERE `mid` in ( $midsStr )");
//                dump($queryMovies);
                return $queryMovies;
            }else{
                return [];
            }

        }
    }

    /**
     * 列表页分页数据
     * @param $nav 主类型
     * @param $type 主题类型
     * @param $area 地区
     * @param $year 年份
     * @param $lang 语言
     * @param $sort 排序
     * @param $pagesize 分页大小
     * @param $pagenum 页码
     * @return array|mixed
     */
    public function getPageMoviesByCondition($nav,$type, $area, $year, $lang, $sort,$pagesize=36,$pagenum)
    {
        $str = [];
        if ($nav) {
            array_push($str, "`nav`='$nav'");
        }
        if ($type) {
            array_push($str, "`type`='$type'");
        }
        if ($area) {
            array_push($str, "`area`='$area'");
        }
        if ($year) {
            array_push($str, "`year`='$year'");
        }
        if ($lang) {
            array_push($str, "`lang`='$lang'");
        }
        //插寻sql
        $sql = implode(" AND ", $str);
        $start=$pagesize*($pagenum-1);
        $query=[];
        if($sql){
            if ($sort == 'desc') {
                $query = Db::query("SELECT `mid`,`update_time` FROM `dybt_movie_classify` WHERE $sql GROUP BY `mid` ORDER BY `update_time` DESC limit $start,$pagesize");
            }
            if ($sort == 'asc'){
                $query = Db::query("SELECT `mid`,`update_time` FROM `dybt_movie_classify` WHERE $sql GROUP BY `mid` ORDER BY `update_time` ASC limit $start,$pagesize");
            }
        }else{
            if ($sort == 'desc') {
                $query = Db::query("SELECT `mid`,`update_time` FROM `dybt_movie_classify`  GROUP BY `mid` ORDER BY `update_time` DESC limit $start,$pagesize");
            }
            if ($sort == 'asc'){
                $query = Db::query("SELECT `mid`,`update_time` FROM `dybt_movie_classify` GROUP BY `mid` ORDER BY `update_time` ASC limit $start,$pagesize");
            }
        }

        if ($query) {
            $midsArr=[];
            foreach ($query as $item){
                array_push($midsArr,$item['mid']);
            }
            $midsStr = implode(",", $midsArr);
            if($midsStr!=""){
                $queryMovies = Db::query("SELECT `mid`, `title`, `cover`, `update_state`,`rating`,`abstract` FROM `dybt_movie` WHERE `mid` in ( $midsStr ) ORDER BY `update_time` DESC");
                return $queryMovies;
            }else{
                return [];
            }
        }
    }

    /**
     * 列表页分页数据的页面总数
     * @param $nav 主类型
     * @param $type 主题类型
     * @param $area 地区
     * @param $year 年份
     * @param $lang 语言
     * @param $pagesize 分页大小
     * @param $pagenum 页码
     * @return array|mixed
     */
    public function getPagenumByCondition($nav,$type, $area, $year, $lang,$pagesize=36){
        $str = [];
        if ($nav) {
            array_push($str, "`nav`='$nav'");
        }
        if ($type) {
            array_push($str, "`type`='$type'");
        }
        if ($area) {
            array_push($str, "`area`='$area'");
        }
        if ($year) {
            array_push($str, "`year`='$year'");
        }
        if ($lang) {
            array_push($str, "`lang`='$lang'");
        }
        //插寻sql
        $sql = implode(" AND ", $str);
        $query=[];

        if($sql){
            $query = Db::query("SELECT COUNT(*) FROM ( SELECT COUNT(*) FROM `dybt_movie_classify` WHERE $sql GROUP BY `mid`) a");
        }else{
            $query = Db::query("SELECT COUNT(*) FROM ( SELECT COUNT(*) FROM `dybt_movie_classify` GROUP BY `mid`) a");
        }

        if ($query) {
            $pagenum=ceil($query[0]["COUNT(*)"]/$pagesize);
            return [$query[0]["COUNT(*)"],$pagenum];
        }else{
            return [0,0];
        }
    }

    /**
     * 详细页面
     * @param $mid
     * @param $type
     * @return array
     */
    public function getMovieDetail($mid,$type){
        // 获取全部信息
        $query=$this->where('mid','=',$mid)->column('mid,title,cover,update_state,actors,story,down_url,visitor,update_time,rating,abstract');
        // 获取分类信息
        $class=Db::query("SELECT `year`,`area`,`lang`,`nav` FROM `dybt_movie_classify` WHERE `mid`=$mid AND `nav`='$type'");
        $arr1=$query[$mid];
        $arr2=$class[0];
        $res=array_merge($arr1,$arr2);
        $res["down_url"]=json_decode($res["down_url"], true);
//        dump($res);
        return $res;
    }

    /**
     * 获取访问量最高的电影
     * @param $num
     * @return mixed
     */
    public function getMostVisitorMovies($num){
        $query=Db::query("SELECT a.mid,a.title,a.cover,rating FROM `dybt_movie` a RIGHT JOIN  `dybt_movie_classify` b ON a.mid = b.mid GROUP BY a.mid ORDER BY a.visitor DESC LIMIT $num");
//        dump($query);
        return $query;
    }

    /**
     * 随机推荐
     * @param $num 推荐数量
     * @return mixed
     */
    public function getMovieByRand($num){
        $query=Db::query("SELECT `mid`, `title`, `cover`,`rating`, `abstract`,`update_state` FROM `dybt_movie` order by rand() limit $num");
//        dump($query);
        return $query;
    }

    /**
     * 访问量+1
     * @param $mid
     */
    public function addVistorNum($mid){
        $res=Db::query("update dybt_movie set visitor=visitor+1 where mid = $mid");
    }

    public function addDownloadNum($mid){
        $res=Db::query("update dybt_movie set down_num=down_num+1 where mid = $mid");
    }

    /**
     * 根据关键词查询
     * @param $name
     * @param $page
     * @param $pagesize
     * @return array|mixed
     */
    public function serachByName($name,$page,$pagesize){
        if($page>0){
            $res=[];
            $start=($page-1)*$pagesize;
            $res=Db::query("SELECT * FROM `dybt_movie` WHERE  `title` LIKE \"%$name%\" order by `update_time` desc LIMIT $start,$pagesize");
            return $res;
        }
    }

    public function pagesBySerach($name,$pagesize){
        $total=0;
        if($name){
            $res=Db::query("SELECT COUNT(*) FROM `dybt_movie` WHERE  `title` LIKE \"$name%\"");
            $total=$res[0]["COUNT(*)"];
        }
        return ceil($res[0]["COUNT(*)"]/$pagesize);
    }

}
