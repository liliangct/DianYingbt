<?php

namespace app\home\model;

use think\Model;

class MovieClassify extends Model
{
    public function getMovieType($mid){
        $res=$this->where('mid','=',$mid)->column('nav');
        return $res[0];
    }
}
