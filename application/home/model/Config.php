<?php
namespace app\home\model;
use think\Model;

class Config extends Model
{
    /**
     * 获取网站配置信息
     * return array
     */
    public function getWebHeaderConfig(){
        $config=$this->find()->column("id,base_url,site_name,logo,keywords,description");
        return $config[1];
    }

    /**
     * 获取网站底部公共信息
     * @return array
     */
    public function getWebFooterConfig(){
        $config=$this->find()->column("id,stats_code,authorities,copyright,email,site_name,base_url");
        return $config[1];
    }
}
