<?php
namespace app\home\model;
use think\Model;

class Nav extends Model
{
    /**
     * 获取所有的导航元素
     * @return array
     */
    public function getAllNav()
    {
        $res=$this->where('isdel' ,">=",0)->column('id,nav,url');
        return $res;
    }

}
