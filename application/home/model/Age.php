<?php
namespace app\home\model;
use think\Model;

class Age extends Model
{
    /**
     * 获取最近的10个年代
     * $num 获取数量
     */
    public function getAll($num=15,$type){
        if($type){
            $quert=$this->where('nav',"=","0")
                        ->where('nav',"=",$type)
                        ->group('title')
                        ->order('title desc')
                        ->limit($num)
                        ->column('title');
        }else{
            $quert=$this->where('nav',"=","0")
                        ->group('title')
                        ->order('title desc')
                        ->limit($num)
                        ->column('title');
        }
        return $quert;
    }
}
