<?php
namespace app\home\model;
use think\Model;

class Lang extends Model
{
    /**
     * 获取所有语言类型
     * @param $type
     * @return array
     */
    public function getAll($type){
        if($type){
            $quert=$this->where('nav',"=","0")
                ->where('nav',"=",$type)
                ->group('title')
                ->column('title');
        }else{
            $quert=$this->where('nav',"=","0")
                ->group('title')
                ->column('title');
        }
        return $quert;
    }
}
