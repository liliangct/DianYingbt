<?php
namespace app\home\controller;

class Index
{
    public function index()
    {
        /*获取首页资源内容*/
        // 资源的配置
        $config=[
            "hotMovies"=>["电影","电视剧","动漫"],
            "movies"=>["欧美","大陆","香港","韩国"],
            "dianshiju"=>["欧美","大陆","香港","韩国"],
            "dongman"=>["欧美","大陆","日本"],
        ];
        $data=[];

        // 最热门电影 电影,电视剧,动漫类型
        foreach ($config["hotMovies"] as $item){
            $data["hot"][$item]=model('Movie')->getMoviesByCondition($item,"","", "", "", "desc");
        }
        // 电影栏目下 欧美,大陆,日韩,港台
        foreach ($config["movies"] as $item){
            $data["movies"][$item]=model('Movie')->getMoviesByCondition("电影","","$item", "", "", "desc");
        }
        // 电视剧栏目下 欧美,大陆,日韩,港台
        foreach ($config["dianshiju"] as $item){
            $data["dianshiju"][$item]=model('Movie')->getMoviesByCondition("电视剧","","$item", "", "", "desc");
        }
        // 动漫栏目下 欧美,大陆,日韩,港台
        foreach ($config["dongman"] as $item){
            $data["dongman"][$item]=model('Movie')->getMoviesByCondition("动漫","","$item", "", "", "desc");
        }
        // 最新跟新
        $data["uptodate_all"]=model('Movie')->getMoviesByCondition("","","", "", "", "desc",11);
        // 电影最新
        $data["uptodate_movies"]=model('Movie')->getMoviesByCondition("电影","","", "", "", "desc",11);
        // 电视剧最新
        $data["uptodate_dianshi"]=model('Movie')->getMoviesByCondition("电视剧","","", "", "", "desc",11);
        // 动漫最新
        $data["uptodate_dongman"]=model('Movie')->getMoviesByCondition("动漫","","", "", "", "desc",11);
//        dump($data);
        $data["config"]=$config;
        return view('index/index',['data'=>$data]);
    }
}
