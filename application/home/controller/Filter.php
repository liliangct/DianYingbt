<?php
namespace app\home\controller;
use think\Request;

class Filter
{
    public function index()
    {
        if (Request::instance()->isGet()){
            $nav=inject_check(Request::instance()->param('type'));
            $params=[
                "type"=>$nav,
                "class"=>inject_check(Request::instance()->param('class')),
                "year"=>inject_check(Request::instance()->param('year')),
                "area"=>inject_check(Request::instance()->param('area')),
                "lang"=>inject_check(Request::instance()->param('lang')),
                "sort"=>inject_check(Request::instance()->param('sort')),
                "page"=>inject_check(Request::instance()->param('page'))
            ];
            if($params['sort']==""){
                $params['sort']='desc';
            }
            if($params['page']==""){
                $params['page']=1;
            }
            $nav=$params['type'];
            $class=$params['class'];
            $area=$params['area'];
            $year=$params['year'];
            $lang=$params['lang'];
            $sort=$params['sort'];
            $pagesize=36;
            $pagenum=$params['page'];
            if($pagenum<0){
                $pagenum=-$pagenum;
                $params['page']=$pagenum;
            }
            $query=model('Movie')->getPageMoviesByCondition($nav,$class, $area, $year, $lang, $sort,$pagesize,$pagenum);
            $pageInfo=model('Movie')->getPagenumByCondition($nav,$class, $area, $year, $lang,36);
            // 获取选择项
            $navLists=model("Nav")->getAllNav();
            $ageLists=model("Age")->getAll(15,"");
            $classLists=model("Classify")->getAll("");
            $countryLists=model("Country")->getAll("");
            $langLists=model("Lang")->getAll("");


            $result=[
                "pagelists"=>$query,
                "totalpage"=>$pageInfo[1],
                "totalnum"=>$pageInfo[0],
                "navLists"=>$navLists,
                "ageLists"=>$ageLists,
                "classLists"=>$classLists,
                "countryLists"=>$countryLists,
                "langLists"=>$langLists,
                "params"=>$params
            ];
//            dump($query);
            return view('filter/index',['data'=>$result]);
        }
    }
}
