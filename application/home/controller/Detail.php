<?php
namespace app\home\controller;
use \think\Request;

class Detail
{
    public function index()
    {
        $mid=inject_check(Request::instance()->param('movie'));
        $type=inject_check(Request::instance()->param('type'));
        if($mid){
            $data=[];
            if(!$type){
                $type=model("MovieClassify")->getMovieType($mid);
            }
            model("Movie")->addVistorNum($mid);
            $data["detail"]=model('Movie')->getMovieDetail($mid,$type);
            $data["hot"]=model('Movie')->getMostVisitorMovies(10);
            $data["uptodate"]=model('Movie')->getMoviesByCondition($type,"","", "", "", "desc",10);
            $data["rand"]=model('Movie')->getMovieByRand(5);
            return view('detail/index',['data'=>$data]);
        }else{
            return;
        }
    }

    /**
     * 统计下载量
     */
    public function downloadNum()
    {
        if (Request::instance()->isPost()){
            $mid=Request::instance()->param('movie');
            model("Movie")->addDownloadNum($mid);
        }
    }
}
