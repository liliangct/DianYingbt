<?php
namespace app\home\controller;
use think\Request;
use think\response\Redirect;

class Search
{
    public function index(){
        $keywords=inject_check(Request::instance()->param('keywords'));
        $page=inject_check(Request::instance()->param('page'));
        if($keywords){
            if(!$page){
                $page=1;
            }
            $lists=model("Movie")->serachByName($keywords,$page,10);
            $total=model("Movie")->pagesBySerach($keywords,10);
            $data=[
                "lists"=>$lists,
                "total"=>$total,
                "page"=>$page,
                "keywords"=>$keywords
            ];
            return view('search/index',["data"=>$data]);
        }else{
            return Redirect(url("home/index/index"));
        }
    }

}